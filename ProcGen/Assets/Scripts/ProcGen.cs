﻿using UnityEngine;

public class ProcGen : MonoBehaviour {
	private MeshFilter filter;
	private MeshRenderer myRenderer;

	[Range(1, 50)]
	public int width = 1;
	[Range(1, 50)]
	public int height = 1;

	public int resolution = 256;
	[Range(0.001f, 50f)]
	public float scale = 2.0f;
	[Range(1, 10)]
	public int octave = 1;
	[Range(0.01f, 5f)]
	public float lacunarity = 2f;
	[Range(0.01f, 5f)]
	public float persistance = .5f;

	public float xOffset = 0.0f;
	public float yOffset = 0.0f;

	private void Start()
	{
		filter = GetComponent<MeshFilter>();
		myRenderer = GetComponent<MeshRenderer>();
		GenerateMesh(width, height);
	}

	private void GenerateMesh(int width, int height)
	{
		float[,] noise = PerlinNoise.FractalNoise(width, height, scale, octave, lacunarity, persistance, xOffset, yOffset);
		MeshData myMesh = PerlinNoise.GenerateTerrainMesh(noise);
		filter.mesh = myMesh.CreateMesh();
	}

	private void Update()
	{
		
	}
}
