﻿using UnityEngine;

public class PerlinNoise : MonoBehaviour
{
	public static float[,] FractalNoise(int width, int height, float scale, int octaves, float lacunarity, float persistance, float xOffset, float yOffset)
	{
		float[,] result = new float[width, height];

		Random.InitState(42);
		float stepSize = 1f;
		float minNoiseHeight = 0f;
		float maxNoiseHeight = 0f;

		for (int y = 0; y < width; y++)
		{
			for (int x = 0; x < height; x++)
			{
				float noiseHeight = 0.0f;
				float frequency = 1.0f;
				float amplitude = 1.0f;

				for (int oct = 0; oct < octaves; oct++)
				{
					float calcX = x / scale * stepSize * frequency + xOffset;
					float calcY = y / scale * stepSize * frequency + yOffset;

					float perlin = Mathf.PerlinNoise(calcX, calcY);
					noiseHeight += perlin * amplitude;

					frequency *= lacunarity;
					amplitude *= persistance;
				}

				result[x, y] = noiseHeight;
				if (noiseHeight < minNoiseHeight)
				{
					minNoiseHeight = noiseHeight;
				}
				else if (noiseHeight > maxNoiseHeight)
				{
					maxNoiseHeight = noiseHeight;
				}
			}
		}

		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				result[x, y] = Mathf.InverseLerp(minNoiseHeight, maxNoiseHeight, result[x, y]);
			}
		}

		return result;
	}

	public static MeshData GenerateTerrainMesh(float[,] heightMap)
	{
		int width = heightMap.GetLength(0);
		int height = heightMap.GetLength(1);
		float topLeftX = (width - 1) / -2f;
		float topLeftZ = (height - 1) / 2f;

		MeshData meshData = new MeshData(width, height);
		int vertexIndex = 0;

		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{

				meshData.vertices[vertexIndex] = new Vector3(topLeftX + x, heightMap[x, y], topLeftZ - y);
				meshData.uvs[vertexIndex] = new Vector2(x / (float)width, y / (float)height);

				if (x < width - 1 && y < height - 1)
				{
					meshData.AddTriangle(vertexIndex, vertexIndex + width + 1, vertexIndex + width);
					meshData.AddTriangle(vertexIndex + width + 1, vertexIndex, vertexIndex + 1);
				}

				vertexIndex++;
			}
		}

		return meshData;
	}
}

public class MeshData
{
	public Vector3[] vertices;
	public int[] triangles;
	public Vector2[] uvs;

	int triangleIndex;

	public MeshData(int meshWidth, int meshHeight)
	{
		vertices = new Vector3[meshWidth * meshHeight];
		uvs = new Vector2[meshWidth * meshHeight];
		triangles = new int[(meshWidth - 1) * (meshHeight - 1) * 6];
	}

	public void AddTriangle(int a, int b, int c)
	{
		triangles[triangleIndex] = a;
		triangles[triangleIndex + 1] = b;
		triangles[triangleIndex + 2] = c;
		triangleIndex += 3;
	}

	public Mesh CreateMesh()
	{
		Mesh mesh = new Mesh();
		mesh.vertices = vertices;
		mesh.triangles = triangles;
		mesh.uv = uvs;
		mesh.RecalculateNormals();
		return mesh;
	}
}