﻿using UnityEngine;

public class WallProcGen : MonoBehaviour
{
	private MeshFilter meshFilter;
	private MeshRenderer meshRenderer;

	[Range(1, 512)]
	public int resolution = 1;

	private int GetVerticies { get { return (resolution + 1) * (resolution + 1); } }
	private int TrianglePoints { get { return 6 * resolution * resolution + ((resolution - 1) * 6); } }

	private void Start()
	{
		meshFilter = GetComponent<MeshFilter>();
		meshRenderer = GetComponent<MeshRenderer>();
		Mesh mesh = GenerateMesh();
		meshFilter.mesh = mesh;
	}

	private Mesh GenerateMesh()
	{
		Mesh mesh = new Mesh();

		Vector3[] vertices = GenerateVertices();
		mesh.vertices = vertices;

		int[] tri = GenerateTriangles();
		mesh.triangles = tri;

		Vector2[] uvs = GenerateUvs();
		mesh.uv = uvs;

		mesh.RecalculateNormals();

		return mesh;
	}

	private Vector3[] GenerateVertices()
	{
		Vector3[] vertices = new Vector3[GetVerticies];

		int indexAt = 0;
		for (int x = 0; x <= resolution; x++)
		{
			for (int y = 0; y <= resolution; y++)
			{
				vertices[indexAt] = new Vector3(y, x, 0);
				indexAt++;
			}
		}

		return vertices;
	}

	private int[] GenerateTriangles()
	{
		int[] tri = new int[TrianglePoints];
		int indexAt = 0;

		for (int i = 0; i < resolution * resolution; i++)
		{
			tri[indexAt] = i;
			indexAt++;

			tri[indexAt] = i + resolution + 1;
			indexAt++;

			tri[indexAt] = i + 1;
			indexAt++;

			tri[indexAt] = i + resolution + 1;
			indexAt++;

			tri[indexAt] = i + resolution + 2;
			indexAt++;

			tri[indexAt] = i + 1;
			indexAt++;
		}

		return tri;
	}

	private Vector2[] GenerateUvs()
	{
		Vector2[] uvs = new Vector2[GetVerticies];

		int indexAt = 0;

		for (int x = 0; x <= resolution; x++)
		{
			for (int y = 0; y <= resolution; y++)
			{
				uvs[indexAt] = new Vector2(y, x);
				indexAt++;
			}
		}

		return uvs;
	}

	private void test()
	{

		for (int x = 0; x < resolution - 1; x++)
		{
			for (int y = 0; y < resolution - 1; y++)
			{
				//tri[indexAt] = indexAt;
				//tri[indexAt + 1] = indexAt + resolution + 1;
				//tri[indexAt + 2] = indexAt + resolution;
				//indexAt += 3;
				//tri[indexAt + 3] = indexAt + resolution + 1;
				//tri[indexAt + 4] = indexAt;
				//tri[indexAt + 5] = indexAt + 1;
			}
		}
	}
}
